//
//  main.m
//  HiStar
//
//  Created by 晴天 on 2019/5/15.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
