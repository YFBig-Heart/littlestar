//
//  YFiAdTimeViewController.m
//  LittleStarfish
//
//  Created by 晴天 on 2019/5/12.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "YFiAdTimeViewController.h"

@interface YFiAdTimeViewController ()

@property (nonatomic,strong)UIButton *timerBtn;
@property (nonatomic,strong)UIImageView *iadimageView;

@end

@implementation YFiAdTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
}
- (void)setUpUI {
    self.iadimageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LaunchScreen_V"]];
    self.timerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.timerBtn.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    [self.timerBtn setTitle:@"3s" forState:UIControlStateNormal];
    [self.view addSubview:self.timerBtn];

}



@end
