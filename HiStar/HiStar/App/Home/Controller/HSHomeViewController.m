//
//  HSHomeViewController.m
//  HiStar
//
//  Created by 晴天 on 2019/5/15.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "HSHomeViewController.h"
#import "UIButton+ImageTitleStyle.h"

@interface HSHomeViewController ()
@property (weak, nonatomic) IBOutlet UIButton *baseFunctionBtn;
@property (weak, nonatomic) IBOutlet UIButton *programmeBtn;
@property (weak, nonatomic) IBOutlet UIButton *manualBtn;
@property (weak, nonatomic) IBOutlet UIButton *myDesginBtn;

@end

@implementation HSHomeViewController

+(instancetype)homeViewController {
    HSHomeViewController *homevc = [[HSHomeViewController alloc] initWithNibName:@"HSHomeViewController" bundle:nil];
    return homevc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUi];
    
}
- (void)setUpUi {
    [self.baseFunctionBtn setTitle:@"基本功能" forState:UIControlStateNormal];
    [self.programmeBtn setTitle:@"编程模式" forState:UIControlStateNormal];
    [self.manualBtn setTitle:@"搭建手册" forState:UIControlStateNormal];
    [self.myDesginBtn setTitle:@"我的创作" forState:UIControlStateNormal];
    
    
    [self.baseFunctionBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
    [self.programmeBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
    [self.manualBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
    [self.myDesginBtn setButtonImageTitleStyle:(ButtonImageTitleStyleTop) padding:3];
}

#pragma mark - action
//基本功能
- (IBAction)baseFunctionBtnClick:(id)sender {
}

- (IBAction)programmeBtnClick:(id)sender {
}
- (IBAction)manualBtnClick:(id)sender {
}
- (IBAction)myDesginBtnClick:(id)sender {
}

- (IBAction)blueToothBtnClick:(id)sender {
}
- (IBAction)settingBtnClick:(id)sender {
}



@end
