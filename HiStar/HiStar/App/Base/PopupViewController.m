//
//  PopupViewController.m
//  CoolMove
//
//  Created by CA on 14-8-23.
//  Copyright (c) 2014年 CA. All rights reserved.
//

#import "PopupViewController.h"


static CGFloat const kCornerRadius = 10.0f;
static CGFloat const kBorderWidth = 0.0f;

@interface PopupViewController ()<CAAnimationDelegate>

@end

@implementation PopupViewController
{
    GradientView *_gradientView;
}

#pragma mark - View LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    self.popupView.layer.cornerRadius = kCornerRadius;
    self.popupView.layer.borderColor = [UIColor colorWithWhite:1.0f alpha:0.5f].CGColor;
    self.popupView.layer.borderWidth = kBorderWidth;
    self.popupView.clipsToBounds = YES;

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (self.isTapCoverViewDismiss) {
        UITouch *tocuh = [touches anyObject];
        if (![tocuh.view isDescendantOfView:self.popupView]) {
            [self dismissFromParentViewControllerWithAnimationType:DetailViewControllerAnimationTypeFade];
        }
    }
    
}


#pragma mark - IBAction

- (IBAction)close:(id)sender {
    [self dismissFromParentViewControllerWithAnimationType:DetailViewControllerAnimationTypeFade];
}

#pragma mark - Private Method

- (void)presentInParentViewController:(UIViewController *)parentViewController animationType:(DetailViewControllerPresentAnimationType)animationType
{
    if (animationType == DetailViewControllerPresentAnimationTypeLeft) {
        
        _gradientView = [[GradientView alloc] initWithFrame:parentViewController.view.bounds];
        [parentViewController.view addSubview:_gradientView];
        
        self.view.frame = parentViewController.view.bounds;
        [parentViewController.view addSubview:self.view];
        [parentViewController addChildViewController:self];
        
        CAKeyframeAnimation *moveAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.x"];
        
        moveAnimation.duration = 0.3;
        moveAnimation.delegate = self;
        moveAnimation.values = @[@160.0, @0.0];
        moveAnimation.keyTimes = @[@0.0, @1.0];
        moveAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        
        [self.view.layer addAnimation:moveAnimation forKey:@"moveAnimation"];
        
        CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        fadeAnimation.fromValue = @0.0f;
        fadeAnimation.toValue = @1.0f;
        fadeAnimation.duration = 0.2;
        [_gradientView.layer addAnimation:fadeAnimation forKey:@"fadeAnimation"];

        
    } else if (animationType == DetailViewControllerPresentAnimationTypeUp) {
        
        _gradientView = [[GradientView alloc] initWithFrame:parentViewController.view.bounds];
        [parentViewController.view addSubview:_gradientView];
        
        self.view.frame = parentViewController.view.bounds;
        [parentViewController.view addSubview:self.view];
        [parentViewController addChildViewController:self];
        
        CAKeyframeAnimation *moveAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.y"];
        
        moveAnimation.duration = 0.3;
        moveAnimation.delegate = self;
        moveAnimation.values = @[@160.0, @0.0];
        moveAnimation.keyTimes = @[@0.0, @1.0];
        moveAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        
      //  [self.view.layer addAnimation:moveAnimation forKey:@"moveAnimation"];
        [self.popupView.layer addAnimation:moveAnimation forKey:@"moveAnimation"];
        
        CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        fadeAnimation.fromValue = @0.0f;
        fadeAnimation.toValue = @1.0f;
        fadeAnimation.duration = 0.2;
        [_gradientView.layer addAnimation:fadeAnimation forKey:@"fadeAnimation"];
        
    } else if (animationType == DetailViewControllerPresentAnimationTypeDown) {
      
        _gradientView = [[GradientView alloc] initWithFrame:parentViewController.view.bounds];
        [parentViewController.view addSubview:_gradientView];
        
        self.view.frame = parentViewController.view.bounds;
        [parentViewController.view addSubview:self.view];
        [parentViewController addChildViewController:self];
        
        CAKeyframeAnimation *moveAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.y"];
        
        moveAnimation.duration = 0.3;
        moveAnimation.delegate = self;
        moveAnimation.values = @[@(-160.0), @0.0];
        moveAnimation.keyTimes = @[@0.0, @1.0];
        moveAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];

//        [self.view.layer addAnimation:moveAnimation forKey:@"moveAnimation"];
        [self.popupView.layer addAnimation:moveAnimation forKey:@"moveAnimation"];
        
        CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        fadeAnimation.fromValue = @0.0f;
        fadeAnimation.toValue = @1.0f;
        fadeAnimation.duration = 0.2;
        [_gradientView.layer addAnimation:fadeAnimation forKey:@"fadeAnimation"];
        
    } else if (animationType == DetailViewControllerPresentAnimationTypeFade) {
        
        _gradientView = [[GradientView alloc] initWithFrame:parentViewController.view.bounds];
        [parentViewController.view addSubview:_gradientView];
        
        self.view.frame = parentViewController.view.bounds;
        [parentViewController.view addSubview:self.view];
        [parentViewController addChildViewController:self];
        
        CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
        
        bounceAnimation.duration = 0.4;
        bounceAnimation.delegate = self;
        
        bounceAnimation.values = @[@2.0, @1.0 ];
        bounceAnimation.keyTimes = @[@0.0, @1.0 ];
        
        bounceAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        
        [self.view.layer addAnimation:bounceAnimation forKey:@"bounceAnimation"];
        
        CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        fadeAnimation.fromValue = @0.0f;
        fadeAnimation.toValue = @1.0f;
        fadeAnimation.duration = 0.4;
        [_gradientView.layer addAnimation:fadeAnimation forKey:@"fadeAnimation"];
        
    } else if (animationType == DetailViewControllerPresentAnimationTypeFadeUp) {
        
        _gradientView = [[GradientView alloc] initWithFrame:parentViewController.view.bounds];
        [parentViewController.view addSubview:_gradientView];
        
        self.view.frame = parentViewController.view.bounds;
        [parentViewController.view addSubview:self.view];
        [parentViewController addChildViewController:self];
        
        CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        fadeAnimation.fromValue = @0.0f;
        fadeAnimation.toValue = @1.0f;
        fadeAnimation.duration = 0.3;
        [self.view.layer addAnimation:fadeAnimation forKey:@"fadeAnimation"];
        [_gradientView.layer addAnimation:fadeAnimation forKey:@"fadeAnimation"];
        
    } else {
        [self presentInParentViewController:parentViewController];
    }
}

- (void)presentInParentViewController:(UIViewController *)parentViewController
{
    _gradientView = [[GradientView alloc] initWithFrame:parentViewController.view.bounds];
    [parentViewController.view addSubview:_gradientView];
    
    self.view.frame = parentViewController.view.bounds;
    [parentViewController.view addSubview:self.view];
    [parentViewController addChildViewController:self];
    
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    bounceAnimation.duration = 0.4;
    bounceAnimation.delegate = self;
    
    bounceAnimation.values = @[ @0.7, @1.2, @0.9, @1.0 ];
    bounceAnimation.keyTimes = @[ @0.0, @0.334, @0.666, @1.0 ];
    
    bounceAnimation.timingFunctions = @[
                                        [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                        [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                        [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [self.view.layer addAnimation:bounceAnimation forKey:@"bounceAnimation"];
    
    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeAnimation.fromValue = @0.0f;
    fadeAnimation.toValue = @1.0f;
    fadeAnimation.duration = 0.2;
    [_gradientView.layer addAnimation:fadeAnimation forKey:@"fadeAnimation"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [self didMoveToParentViewController:self.parentViewController];
}

- (void)dismissFromParentViewControllerWithAnimationType:(DetailViewControllerAnimationType)animationType
{
    [self willMoveToParentViewController:nil];
    [UIView animateWithDuration:0.3 animations:^{
        if (animationType == DetailViewControllerAnimationTypeSlide) {
            CGRect rect = self.view.bounds;
            rect.origin.y += rect.size.height;
            self.view.frame = rect;
        } else if (animationType == DetailViewControllerAnimationTypeSlideRight) {
            CGRect rect = self.view.bounds;
            rect.origin.x += rect.size.width;
            self.view.frame = rect;
        } else {
            self.view.alpha = 0.0f;
        }
        self->_gradientView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        [self->_gradientView removeFromSuperview];
    }];
}

@end


@implementation GradientView

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
        self.backgroundColor = [UIColor clearColor];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"dealloc %@", self);
}

- (void)drawRect:(CGRect)rect
{
    const CGFloat components[8] = { 0.0f, 0.0f, 0.0f, 0.3f, 0.0f, 0.0f, 0.0f, 0.7f };
    const CGFloat locations[2] = { 0.0f, 1.0f };

    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(space, components, locations, 2);
    CGColorSpaceRelease(space);

    CGFloat x = CGRectGetMidX(self.bounds);
    CGFloat y = CGRectGetMidY(self.bounds);
    CGPoint point = CGPointMake(x, y);
    CGFloat radius = MAX(x, y);

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawRadialGradient(context, gradient, point, 0, point, radius, kCGGradientDrawsAfterEndLocation);
    CGGradientRelease(gradient);
}

@end
