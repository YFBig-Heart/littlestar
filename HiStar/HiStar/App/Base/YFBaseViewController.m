//
//  YFBaseViewController.m
//  CoollangTennisBall
//
//  Created by Coollang on 2017/9/12.
//  Copyright © 2017年 Coollang-YF. All rights reserved.
//

#import "YFBaseViewController.h"


@interface YFBaseViewController ()

@end

@implementation YFBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}


@end
