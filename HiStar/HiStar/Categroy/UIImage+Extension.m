//
//  UIImage+Extension.m
//  FOXLive
//
//  Created by mac on 16/8/3.
//  Copyright © 2016年 Foxconn. All rights reserved.
//

#import "UIImage+Extension.h"

@implementation UIImage (Extension)

+ (UIImage *)getScreenShot {
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    // 2.开启绘画 size:大小 opaue：透明度 scale:缩放
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(kYFScreenWidth, kYFScreenHeight), NO, 1);
    // 3. 让window进行绘画
    [window drawViewHierarchyInRect:CGRectMake(0, 0, kYFScreenWidth, kYFScreenHeight) afterScreenUpdates:NO];
    // 4.获取image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    // 5. 关闭绘画
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *) imageWithTintColor:(UIColor *)tintColor alpha:(CGFloat)alpha
{
    return [self imageWithTintColor:tintColor blendMode:kCGBlendModeDestinationIn alpha:alpha];
}

- (UIImage *) imageWithTintColor:(UIColor *)tintColor blendMode:(CGBlendMode)blendMode alpha:(CGFloat)alpha {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0f);
    [tintColor setFill];
    CGRect bounds = CGRectMake(0, 0, self.size.width, self.size.height);
    UIRectFill(bounds);
    if (alpha < 0 || alpha > 1) {
        alpha = 1.0;
    }
    //Draw the tinted image in context
    [self drawInRect:bounds blendMode:blendMode alpha:alpha];
    if (blendMode != kCGBlendModeDestinationIn) {
        [self drawInRect:bounds blendMode:kCGBlendModeDestinationIn alpha:alpha];
    }
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tintedImage;
}



@end
