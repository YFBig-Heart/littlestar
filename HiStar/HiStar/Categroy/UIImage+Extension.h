//
//  UIImage+Extension.h
//  FOXLive
//
//  Created by mac on 16/8/3.
//  Copyright © 2016年 Foxconn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYPhotoGroupView.h"

@interface UIImage (Extension)

+ (UIImage *)getScreenShot;
- (UIImage *) imageWithTintColor:(UIColor *)tintColor blendMode:(CGBlendMode)blendMode alpha:(CGFloat)alpha;
- (UIImage *) imageWithTintColor:(UIColor *)tintColor alpha:(CGFloat)alpha;


@end
