//
//  UIImage+PetExtension.m
//  Petcome
//
//  Created by petcome on 2019/2/14.
//  Copyright © 2019 yunfei. All rights reserved.
//

#import "UIImage+PetExtension.h"
#import <ImageIO/ImageIO.h>

@implementation UIImage (PetExtension)
+ (UIImage *)animatedGIFWithData:(NSData *)data {
    if (!data) {
        return nil;
    }
    //通过CFData读取gif文件的数据
    CGImageSourceRef source = CGImageSourceCreateWithData((__bridge CFDataRef)data, NULL);
    //获取gif文件的帧数
    size_t count = CGImageSourceGetCount(source);
    UIImage *animatedImage;
    if (count <= 1) {
        animatedImage = [[UIImage alloc] initWithData:data];
    }
    else {//大于一张图片时
        NSMutableArray *images = [NSMutableArray array];
        //设置gif播放的时间
        NSTimeInterval duration = 0.0f;
        for (size_t i = 0; i < count; i++) {
            //获取gif指定帧的像素位图
            CGImageRef image = CGImageSourceCreateImageAtIndex(source, i, NULL);
            if (!image) {
                continue;
            }
            //获取每张图的播放时间
            duration += [self frameDurationAtIndex:i source:source];
            [images addObject:[UIImage imageWithCGImage:image scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp]];
            CGImageRelease(image);
        }
        if (!duration) {//如果播放时间为空
            duration = (1.0f / 10.0f) * count;
        }
        animatedImage = [UIImage animatedImageWithImages:images duration:duration];
    }
    CFRelease(source);
    return animatedImage;
}

+ (float)frameDurationAtIndex:(NSUInteger)index source:(CGImageSourceRef)source {
    float frameDuration = 0.1f;
    //获取这一帧图片的属性字典
    CFDictionaryRef cfFrameProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil);
    NSDictionary *frameProperties = (__bridge NSDictionary *)cfFrameProperties;
    //获取gif属性字典
    NSDictionary *gifProperties = frameProperties[(NSString *)kCGImagePropertyGIFDictionary];
    //获取这一帧持续的时间
    NSNumber *delayTimeUnclampedProp = gifProperties[(NSString *)kCGImagePropertyGIFUnclampedDelayTime];
    if (delayTimeUnclampedProp) {
        frameDuration = [delayTimeUnclampedProp floatValue];
    }
    else {
        NSNumber *delayTimeProp = gifProperties[(NSString *)kCGImagePropertyGIFDelayTime];
        if (delayTimeProp) {
            frameDuration = [delayTimeProp floatValue];
        }
    }
    //如果帧数小于0.1,则指定为0.1
    if (frameDuration < 0.011f) {
        frameDuration = 0.100f;
    }
    CFRelease(cfFrameProperties);
    return frameDuration;
}

- (UIImage *)animatedImageByScalingAndCroppingToSize:(CGSize)size {
    if (CGSizeEqualToSize(self.size, size) || CGSizeEqualToSize(size, CGSizeZero)) {
        return self;
    }
    CGSize scaledSize = size;
    CGPoint thumbnailPoint = CGPointZero;
    //获取较大的缩放比例值,宽高等比缩放
    CGFloat widthFactor = size.width / self.size.width;
    CGFloat heightFactor = size.height / self.size.height;
    CGFloat scaleFactor = (widthFactor > heightFactor) ? widthFactor : heightFactor;
    scaledSize.width = self.size.width * scaleFactor;
    scaledSize.height = self.size.height * scaleFactor;
    //调整位置,使缩放后的图居中
    if (widthFactor > heightFactor) {
        thumbnailPoint.y = (size.height - scaledSize.height) * 0.5;
    }
    else if (widthFactor < heightFactor) {
        thumbnailPoint.x = (size.width - scaledSize.width) * 0.5;
    }
    //遍历self.images, 将图片缩放后导出放入数组
    NSMutableArray *scaledImages = [NSMutableArray array];
    for (UIImage *image in self.images) {
        UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
        [image drawInRect:CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledSize.width, scaledSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        [scaledImages addObject:newImage];
        UIGraphicsEndImageContext();
    }
    return [UIImage animatedImageWithImages:scaledImages duration:self.duration];
}

- (UIImage *)normalizedImage
{
    if (self.imageOrientation == UIImageOrientationUp) return self;

    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    [self drawInRect:(CGRect){0, 0, self.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}
- (UIImage *)fullNormalizedImage {
    if (self.imageOrientation == UIImageOrientationUp) return self;

    CGAffineTransform transform = CGAffineTransformIdentity;

    switch (self.imageOrientation) { case UIImageOrientationDown: case UIImageOrientationDownMirrored: transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height); transform = CGAffineTransformRotate(transform, M_PI); break;

        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;

        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, self.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }

    switch (self.imageOrientation) { case UIImageOrientationUpMirrored: case UIImageOrientationDownMirrored: transform = CGAffineTransformTranslate(transform, self.size.width, 0); transform = CGAffineTransformScale(transform, -1, 1); break;

        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }

    CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height, CGImageGetBitsPerComponent(self.CGImage), 0, CGImageGetColorSpace(self.CGImage), CGImageGetBitmapInfo(self.CGImage)); CGContextConcatCTM(ctx, transform); switch (self.imageOrientation) { case UIImageOrientationLeft: case UIImageOrientationLeftMirrored: case UIImageOrientationRight: case UIImageOrientationRightMirrored:

            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
            break;

        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
            break;
    }

    CGImageRef cgimg = CGBitmapContextCreateImage(ctx); UIImage *img = [UIImage imageWithCGImage:cgimg]; CGContextRelease(ctx); CGImageRelease(cgimg); return img;
}

- (UIImage *)clipImage:(CGFloat)scale
{
    CGFloat width = self.size.width;
    CGFloat height = self.size.height;

    CGRect rect = CGRectMake((width - width / scale) / 2, height / 2 - width / scale / 2, width / scale, width / scale);
    CGImageRef imageRef = self.CGImage;
    CGImageRef imagePartRef = CGImageCreateWithImageInRect(imageRef, rect);
    UIImage *image = [UIImage imageWithCGImage:imagePartRef];
    CGImageRelease(imagePartRef);
    return image;
}
- (UIImage *)clipLeftOrRightImage:(CGFloat)scale {
    CGFloat width = self.size.width;
    CGFloat height = self.size.height;

    CGRect rect = CGRectMake((width - height / scale) / 2, height / 2 - height / scale / 2, height / scale, height / scale);
    CGImageRef imageRef = self.CGImage;
    CGImageRef imagePartRef = CGImageCreateWithImageInRect(imageRef, rect);
    UIImage *image = [UIImage imageWithCGImage:imagePartRef];
    CGImageRelease(imagePartRef);
    return image;
}

- (UIImage *)clipNormalizedImage:(CGFloat)scale {
    CGFloat width = self.size.width;
    CGFloat height = self.size.height;

    CGRect rect = CGRectMake((width - width / scale) / 2, height / 2 - height / scale / 2, width / scale, height / scale);
    CGImageRef imageRef = self.CGImage;
    CGImageRef imagePartRef = CGImageCreateWithImageInRect(imageRef, rect);
    UIImage *image = [UIImage imageWithCGImage:imagePartRef];
    CGImageRelease(imagePartRef);
    return image;
}

- (UIImage *)scaleImagetoScale:(float)scaleSize {
    CGSize newSize = CGSizeMake(self.size.width * scaleSize, self.size.height * scaleSize);
    newSize = CGSizeMake(ceilf(newSize.width), ceilf(newSize.height));
    UIGraphicsBeginImageContext(newSize);
    [self drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (NSData *)compressToDataHX
{
    //    return [self imitationWechatImageCompress:YES];
    NSData *data = UIImageJPEGRepresentation(self, 1.0);
    NSUInteger length = data.length;
    //长图处理
    if ((self.size.height / self.size.width > 2.0)||
        (self.size.width / self.size.height > 2.0)) {
        if (length > 1520 * 1000) {
            CGFloat scale = sqrtf(520 * 1000.0f/length);
            NSLog(@"缩放:%f", scale);
            UIImage *scaleImage = [self scaleImagetoScale:scale];
            data = UIImageJPEGRepresentation(scaleImage, 1.0);
        }
        return data;
    }
    if (length > 520 * 1000) {
        CGFloat scale = sqrtf(520 * 1000.0f/length);
        NSLog(@"缩放:%f", scale);
        UIImage *scaleImage = [self scaleImagetoScale:scale];
        data = UIImageJPEGRepresentation(scaleImage, 0.8);
    }else if (length > 300 * 1000){
        data = UIImageJPEGRepresentation(self, 0.5);
    }
    return data;
}

/*
 http://blog.csdn.net/u014220518/article/details/58136932
 图片尺寸
 宽高均 <= 1280，图片尺寸大小保持不变
 宽或高 > 1280 && 宽高比 <= 2，取较大值等于1280，较小值等比例压缩
 宽或高 > 1280 && 宽高比 > 2 && 宽或高 < 1280，图片尺寸大小保持不变
 宽高均 > 1280 && 宽高比 > 2，取较小值等于1280，较大值等比例压缩
 注：当宽和高均小于1280，并且宽高比大于2时，微信聊天会话和微信朋友圈的处理不一样。
 朋友圈：取较小值等于1280，较大值等比例压缩
 聊天会话：取较小值等于800，较大值等比例压缩
 图片质量
 经过大量的测试，微信的图片压缩质量值 ≈ 0.5
 */
- (NSData *)imitationWechatImageCompress:(BOOL)isMoment {

    // isMoment:-> YES代表朋友圈 NO代表会话框
    CGFloat limValue = isMoment ? 1280:800;

    CGFloat imageW = self.size.width;
    CGFloat imageH = self.size.height;

    //宽高比
    CGFloat scaleWH = MAX(imageH, imageW)/ MIN(imageH, imageW);
    CGSize scaleSize = self.size;
    if (imageW > limValue || imageH > limValue){
        if (scaleWH <= 2) {
            // 取较大值等于1280，较小值等比例压缩
            if (imageH <= imageW) {
                scaleSize = CGSizeMake(limValue, limValue * (imageH / imageW));
            }else {
                scaleSize = CGSizeMake(limValue * (imageW /imageH),limValue);
            }
        }else {
            if (imageW > limValue && imageH > limValue) {
                // 取较小值等于1280，较大值等比例压缩
                if (imageH <= imageW) {
                    scaleSize = CGSizeMake(limValue * (imageW /imageH),limValue);
                }else {
                    scaleSize = CGSizeMake(limValue, limValue * (imageH / imageW));
                }
            }
        }
    }
    UIGraphicsBeginImageContextWithOptions(scaleSize, YES, 0);
    [self drawInRect:CGRectMake(0, 0, scaleSize.width, scaleSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *data = UIImageJPEGRepresentation(newImage, 0.5);
    NSLog(@"%lu",(unsigned long)UIImageJPEGRepresentation(self, 1.0).length);
    NSLog(@"%lu",(unsigned long)data.length);
    return data;
}


@end
