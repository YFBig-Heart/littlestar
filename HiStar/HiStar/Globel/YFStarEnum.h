//
//  YFStarEnum.h
//  LittleStarfish
//
//  Created by 晴天 on 2019/5/11.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YFStarEnum : NSObject
// 性别
typedef NS_ENUM(NSInteger, Gender){
    GenderUnknown = 0,  ///未知
    GenderMale = 1,     ///男
    GenderFemale = 2,   ///女
};

@end

NS_ASSUME_NONNULL_END
