//
//  YFConfigBase.h
//  Petcome
//
//  Created by petcome on 2018/12/24.
//  Copyright © 2018 yunfei. All rights reserved.
//

#ifndef YFConfigBase_h
#define YFConfigBase_h

#import "UIView+Layer.h"
#import "UIView+Frame.h"
#import "UIButton+YFHitRect.h"
#import "YFUIDefine.h"
#import "YFColordefine.h"


#define kCurrentLanguageIschinese [[[NSLocale preferredLanguages] objectAtIndex:0] rangeOfString:@"zh-Hans"].location != NSNotFound


// Bugly
#define kBuglyAppid @"57032d1f0a"
#define kBuglyAppKey @"d73cd61a-a8c8-4a9d-b4ad-3bb8e543caac"


//应用名称
#define kAPPName                [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]
//版本号
#define kAppVersion             [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
//开发版本号
#define kBuildVersion           [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]
//Bundle identifier
#define kAppBundleID            [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]



///系统版本
#define kSystemVersion   ([[UIDevice currentDevice] systemVersion])
#define IOS_Version ([kSystemVersion floatValue])

#define IOS_OR_LATER(version)   (IOS_Version >= version)
#define IOS7_OR_LATER   (IOS_Version >= 7.0) //ios 7
#define IOS8_OR_LATER   (IOS_Version >= 8.0) //ios 8
#define IOS9_OR_LATER    (IOS_Version >= 9.0)  //ios 9
#define IOS10_OR_LATER   (IOS_Version >= 10.0) //ios 10
#define IOS11_OR_LATER   (IOS_Version >= 11.0) //ios 11


/** 强弱引用 **/
#define WS(weakSelf)    __weak __typeof(&*self)   weakSelf  = self;
#define SS(strongSelf)  __strong __typeof(&*self) strongSelf = weakSelf;


//主线程
#define GCDMain(block)       dispatch_async(dispatch_get_main_queue(),block)
///延迟处理
#define GCDAfter(seconds, block) dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)( seconds * NSEC_PER_SEC)),dispatch_get_main_queue(), block)


#endif /* YFConfigBase_h */
