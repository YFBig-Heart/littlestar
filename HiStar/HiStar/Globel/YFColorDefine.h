//
//  YFColorDefine.h
//  Petcome
//
//  Created by petcome on 2018/12/24.
//  Copyright © 2018 yunfei. All rights reserved.
//

#ifndef YFColorDefine_h
#define YFColorDefine_h


///随机颜色
#define RandomColor      [UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1];
//颜色
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
///16进制RGB颜色转换
#define RGB16TOCOLOR(rgb) [UIColor colorWithRed:((float)((rgb & 0xFF0000) >> 16))/255.0 green:((float)((rgb & 0xFF00) >> 8))/255.0 blue:((float)(rgb & 0xFF))/255.0 alpha:1.0]
#define RGBA16TOCOLOR(rgb,alphaValue) [UIColor colorWithRed:((float)((rgb & 0xFF0000) >> 16))/255.0 green:((float)((rgb & 0xFF00) >> 8))/255.0 blue:((float)(rgb & 0xFF))/255.0 alpha:alphaValue]


#define kNavColor RGB16TOCOLOR(0xF3CB35)// 黄色（浅一点)
#define kNavUnableColor RGBA16TOCOLOR(0xF3CB35, 0.5) //黄色失效下的按钮颜色

#define kLightGrayBgColor RGB16TOCOLOR(0xEFF3F6)
#define kNavTextDrakColor RGB16TOCOLOR(0x141414)

#define kMainThemeColor RGB16TOCOLOR(0xF6AB00) //黄色主题色（深一点）
#define kOrangeRedColor RGB16TOCOLOR(0xEC631A) // 底部橘黄色按钮
#define kOrangeRedUnEnableColor RGBA16TOCOLOR(0xEC631A, 0.5) // 底部橘黄色失效状态按钮
#define kTextBlackColor RGB16TOCOLOR(0x141414)
#define kTextDrakGrayColor RGB16TOCOLOR(0x434053) // 深灰色字体
#define kTextGrayColor RGBA16TOCOLOR(0x434053, 0.5) // 浅灰色字体
// 蒙版背景色
#define kMaskViewBgColor [UIColor colorWithWhite:0 alpha:0.4]

#define kTextLightBlueColor RGBA16TOCOLOR(0x59b6d7, 1.0) // 可点击蓝色标签文字颜色

#define kTextGreenColor RGB16TOCOLOR(0x62C0B4)

#endif /* YFColorDefine_h */
