//
//  YFHomeHelper.h
//  LittleStarfish
//
//  Created by 晴天 on 2019/5/13.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YFHomeHelper : NSObject

/**
 1.GCD计时器
 */
- (dispatch_source_t)interiorDCDTimerManage:(int)maxTimeout span:(CGFloat)span reduseTimeBlock:(void(^)(int reduseTime))reduseTime cancelBlock:(void(^)(void))cancelBlock;


@end

NS_ASSUME_NONNULL_END
