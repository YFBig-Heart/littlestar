//
//  YFHomeHelper.m
//  LittleStarfish
//
//  Created by 晴天 on 2019/5/13.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "YFHomeHelper.h"

@implementation YFHomeHelper
/**
 1.GCD计时器
 */
- (dispatch_source_t)interiorDCDTimerManage:(int)maxTimeout span:(CGFloat)span reduseTimeBlock:(void(^)(int reduseTime))reduseTime cancelBlock:(void(^)(void))cancelBlock {
    __block int timeout = maxTimeout;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, dispatch_walltime(NULL, 0), span * NSEC_PER_SEC, 0); // 每span执行一次
    dispatch_source_set_event_handler(timer, ^{ // 定时器执行
        if (timeout == 0) {
            dispatch_source_cancel(timer);
        } else {
            GCDMain(^{
                if (reduseTime) {
                    reduseTime(timeout);
                }
            });
        }
        timeout--;
    });
    dispatch_source_set_cancel_handler(timer, ^{
        // 关闭定时器执行方法
        GCDMain(^{
            if (cancelBlock) {
                cancelBlock();
            }
        });
    });
    // 启动
    dispatch_resume(timer);
    return timer;
}

@end
