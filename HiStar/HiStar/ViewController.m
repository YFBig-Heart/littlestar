//
//  ViewController.m
//  HiStar
//
//  Created by 晴天 on 2019/5/15.
//  Copyright © 2019年 晴天. All rights reserved.
//

#import "ViewController.h"
#import "HSHomeViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    HSHomeViewController *homeVc = [HSHomeViewController homeViewController];
    [self presentViewController:homeVc animated:YES completion:nil];
}

@end
